library(shiny)
library(shinyjs)

# UI & components
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/tabs/tab_variantreport.R")
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/tabs/tab_watchdogreport.R")
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/tabs/tab_pathowinreport.R")
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/tabs/tab_rclones5.R")
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/tabs/tab_lymphocyte.R")
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/tabs/tab_gnxsqc.R")
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/tabs/tab_casedistribution.R")

# Functions
source("/home/watchdog/github_app/rshiny/variant_analysis/modular_app/R/functions.R")


ui <- fluidPage(
		  useShinyjs(),
		    navbarPage("Shell Script Execution",
			       tab_variantreport_UI,
                               tab_watchdogreport_UI,
		      	       tab_pathowinreport_UI,
			       tab_rclone_UI,
			       tab_lymphocyteseq_UI,
			       tab_gnxsqc_UI,
		    	       tab_casedistro_UI)
)


server <- function(input, output, session){

	primepath = "/mnt/NGS_Diagnostik/Patientendaten/WATCHDOG_LANDINGBAY/PRIME" 
	genexuspath = "/mnt/NGS_Diagnostik/Patientendaten/WATCHDOG_LANDINGBAY/GENEXUS"  
	# Render the variable as a reactive output
  	output$path_prime <- renderText({primepath})
	output$path_genexus <- renderText({genexuspath})



	# Tab Variant Report
	vcf_event(input, output, session)
	pyrepair_event(input, output, session)
	# Tab Watchdog
	watchdog_event(input, output, session)
	# Genexus QC
	gnxs_event(input, output, session)
	# Tab Pathowin Report
      	yaml_event(input, output, session)
	pathowinreport_event(input, output, session)

	# Tab S5 download
	rclone_IRzip_event(input, output, session)
	rclone_IRtsv_event(input, output, session)
	dir_rename_event(input, output, session)

	#  Tab Lymphocyte Seq
	lymphocyteseq_event(input, output, session)
	# Tab Case distribution
	case_event(input, output, session)
	add_variants_event(input, output, session)
}

shinyApp(ui, server)
