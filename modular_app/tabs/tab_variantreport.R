#!/usr/bin/R
tab_variantreport_UI = tabPanel("Variant Report",
                                        sidebarLayout(
                              			sidebarPanel(width = 3,
                                           		img(src = "VCF_mountain.png", height = 400, width = 400)
                                          ),

                                        mainPanel(
                                        h1("Variant Report"),
					h3("Generate pyrepair.vcf"),
                                        textInput("geniepath", "Paste path"),
                                        actionButton("pyrepair_execute", "Run script to generate pyrepair.vcf"),
					h3("Generate VariantReport.html"),
					actionButton("genie_execute", "Generate VariantReport.html"),
                                        p(" Directory must contain **pyrepair.vcf** files"),
                                        p(" Currently paths should be specified in UNIX compliant manner, i.e. forward-slashes and starting from /mnt/"),
                                        h4(textOutput("path_prime")),
                                        h4(textOutput("path_genexus"))
					)
					)
)

vcf_event = function(input, output, session){
	observeEvent(
               input$genie_execute, {
               # Get the directory path from the text input

               dir_path = input$geniepath
               system(paste("~/github_app/genie/parallel_genie.sh", dir_path))
	       #file_paths = list.files(path = dir_path, pattern = ".*_Filtered.*vcf", recursive = TRUE, full.names = TRUE)
               #oncomine_paths = list.files(path = dir_path, pattern = "Onco.*.vcf", recursive = TRUE, full.names = TRUE)
               #file_paths = c(file_paths, oncomine_paths)
               # Find all files named "prep_snv.txt" in the directory and its subdirectories
               #lapply(file_paths, genie_script)
               }
	)
}



pyrepair_event = function(input, output, session){
        observeEvent(
               input$pyrepair_execute, {
               # Get the directory path from the text input
               dir_path = input$geniepath
               file_paths = list.files(path = dir_path, pattern = ".*_Filtered.*vcf", recursive = TRUE, full.names = TRUE)
               oncomine_paths = list.files(path = dir_path, pattern = "Onco.*.vcf", recursive = TRUE, full.names = TRUE)
               file_paths = c(file_paths, oncomine_paths)
	       file_paths = grep("pyrepair|gnomadMAF", file_paths, invert = TRUE, value = TRUE)
	       lapply(file_paths, function(x) system(paste("python3 /home/watchdog/github_app/base_pythonrepairsvcf/vcfrepair.py", x)))
               }
        )
}




