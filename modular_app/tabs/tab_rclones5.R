#!/usr/bin/R

tab_rclone_UI =	tabPanel("Download S5 files",
			sidebarLayout(
			      sidebarPanel(width = 3,
					   img(src = "downloadsnake.png", height = 400, width = 400)
                                          ),
                                        mainPanel(
					h1("Download analysis files from Ion Reporter"),
					p("Due to the way that the analysis are saved on the IR, multiple steps are necessary."),
					p("One has 3h to complete the download cycle, since otherwise too many files might get retained"),
					h3("Download zip files"),
                                        withTags({
                                        div(class = "header", checked = NA,
                                        p("Steps for download assistance"),
                                        p(ul(
                                           li("Click on every file (Filtered.zip, Non-Filtered.zip, bam.zip, TSV)"),
                                           li("Wait an appropiate amount of time until files are likely done being processed (or click multiple times)")
                                           )
                                        )
                                        )}),
                                        
                                        actionButton("rclone_IRzip_execute", "Push button to trigger rclone download of zip files"),
                                        
					h3("Rename zip file containing directories"),
					p("Once all .zip files are downloaded, you can start the python script to rename the directories."),
					p("This will rename directories and move zip files with the same ID into corresponding directories"),
					actionButton("dir_rename_execute", "Rename directories and move .zip files"),
					h3("Download TSV files"),
					withTags({
                                        div(class = "header", checked = NA,
                                        p("Step to download TSV files"),
                                        ul(
                                           li("Make sure that the zip file downloads are finished, since the TSV download takes much longer (due to the IonReporter data structure)"),
					   li("TSV files will be downloaded to S5_LANDINGBAY and not moved to directories")
                                        ))}),
                                        actionButton("rclone_IRtsv_execute", "Download TSV files (and get a coffee from upstairs..)")
                                        )
					)
)


rclone_IRzip_event = function(input, output, session){
       	observeEvent(
               input$rclone_IRzip_execute, {
		 system("/home/watchdog/github_app/S5downloads/rclone_cmds.sh")
               }

  )
}
rclone_IRtsv_event = function(input, output, session){
        observeEvent(
               input$rclone_IRtsv_execute, {
                 system("/home/watchdog/github_app/S5downloads/find_cmd.sh")
               }

  )
}

dir_rename_event = function(input, output, session){
        observeEvent(
		     input$dir_rename_execute, {
			     zip_files = list.files(pattern = ".*.zip", full.names = TRUE, path = "/mnt/NGS_Diagnostik/Patientendaten/S5_LANDINGBAY", recursive=TRUE)
			     lapply(zip_files, function(x) system(paste("python3", "/home/watchdog/github_app/FileMgmt/rename_downloads.py", x)))
			     #Sys.sleep(10)
			     #zip_files = list.files(pattern = ".*.zip", full.names = TRUE, path = "/mnt/NGS_Diagnostik/Patientendaten/S5_LANDINGBAY", recursive=TRUE)
			     #lapply(zip_files, function(x) system(paste("python3", "/home/watchdog/github_app/FileMgmt/Modules/extract_zipfile.py", x)))
	       }
  )
}





